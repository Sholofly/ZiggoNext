# DEPRECATED! Horizon EOS Settopbox (Arris DCX960)

This component is deprecated. Please remove this component from your hacs and add the [Arris DCX960](https://github.com/Sholofly/arrisdcx960). Please note there is an additional step required (adding a custom repo) to install that new component!
